﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using KindleNewsPublisher.Classes;
using KindleNewsPublisher.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.ServiceModel.Syndication;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace KindleNewsPublisher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// 
    /// http://kindlegen.s3.amazonaws.com/AmazonKindlePublishingGuidelines.pdf
    /// </summary>
    public partial class MainWindow : Window
    {
        private DateTime timerStart = DateTime.Now;
        private System.Windows.Threading.DispatcherTimer timer = null;
        private AesManaged AesMan = null;
        private String DocumentTitle = "n/a";
        private FileInfo DocumentFileInfo = null;
        private static iTextSharp.text.Rectangle PdfPageSize = PageSize.HALFLETTER;
        private List<RssNews> newsList = new List<RssNews>();
        //private static byte[] AES_KEY =  new byte[] { 181, 195, 250, 146, 216, 191, 237, 113, 152, 254, 112, 46, 24, 43, 241, 99, 26, 79, 161, 252, 220, 213, 128, 17, 92, 119, 113, 72, 249, 205, 93, 153 };
        //private static byte[] AES_IV = new byte[] { 80, 186, 208, 125, 76, 249, 190, 48, 32, 213, 253, 55, 3, 195, 137, 28 };

        public MainWindow()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            Title = Settings.Default.ProgramTitle;
            if (Settings.Default.RssLinks == null)
            {
                Settings.Default.RssLinks = new System.Collections.Specialized.StringCollection();
            }
            FillComboBoxWithRssItems();
            
            AesMan = new AesManaged();
            
            if(Settings.Default.AesKey.Length == 0 || Settings.Default.AesIV.Length == 0)
            {
                AesMan.GenerateKey();
                AesMan.GenerateIV();
                Settings.Default.AesKey = Encoding.ASCII.GetString(AesMan.Key);
                Settings.Default.AesIV = Encoding.ASCII.GetString(AesMan.IV);
                Settings.Default.Save();
                AesMan.Key = GetAesKey();
                AesMan.IV = GetAesIV();
            }
            else
            {
                AesMan.Key = GetAesKey();
                AesMan.IV = GetAesIV();    
            }
            
            timer = new System.Windows.Threading.DispatcherTimer()
            {
                Interval = new TimeSpan(0, 0, 5), // 5 seconds interval
            };
            timer.Tick += timer_Tick;
        }

        byte[] GetAesKey()
        {
            return Encoding.ASCII.GetBytes(Settings.Default.AesKey);
        }

        byte[] GetAesIV()
        {
            return Encoding.ASCII.GetBytes(Settings.Default.AesIV);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Subtract(timerStart).TotalSeconds > 5)
            {
                Title = Settings.Default.ProgramTitle; // Reset Window Title to default
                timer.Stop();
            }
        }

        private void FillComboBoxWithRssItems()
        {
            CmbRssLinks.Items.Clear();
            foreach (String item in Settings.Default.RssLinks)
            {
                CmbRssLinks.Items.Add(item);
            }
        }

        public String SanitizeFileName(String filename)
        {
            Char[] invalidChars = System.IO.Path.GetInvalidFileNameChars();
            foreach (char c in filename.ToCharArray())
            {
                if (invalidChars.Contains(c))
                {
                    filename = filename.Replace(c, '-');
                }
            }
            return filename;
        }

        public Boolean FillListWithRssFeedData(string rssUrl)
        {
            try
            {
                Uri imgUrl = null;
                String authors = "";
                XmlReader reader;
                try
                {
                    reader = XmlReader.Create(rssUrl);
                }
                catch (NotSupportedException ex)
                {
                    MessageBox.Show("Invalid URI!");
                    return false;
                }
                SyndicationFeed sf = SyndicationFeed.Load(reader);
                DocumentTitle = sf.Title.Text;
                DocumentTitle = SanitizeFileName(DocumentTitle);
                newsList.Clear(); // Clear before re-filling the list
                foreach (SyndicationItem si in sf.Items)
                {
                    foreach (var link in si.Links)
                    {
                        if (link.MediaType == MediaTypeNames.Image.Jpeg)
                        {
                            imgUrl = link.Uri;
                        }
                    }
                    foreach (var author in si.Authors)
                    {
                        authors += author.Name + ", ";
                    }
                    if (authors.Length > 2)
                    {
                        authors = authors.Substring(0, authors.Length - 2);
                    }
                    newsList.Add(new RssNews()
                    {
                        Title = StripHTML(si.Title.Text),
                        Author = StripHTML(authors),
                        Description = StripHTML(si.Summary.Text).TrimStart(new char[] { ' ', '\n', '\r' }),
                        PublicationDate = si.PublishDate,
                        ImageUrl = imgUrl
                    });
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show(ex.ToString());
                Console.WriteLine(ex);
                return false;
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            catch (XmlException ex)
            {
                MessageBox.Show("Invalid Feed!");
                return false;
            }
            return true;
        }

        private static string StripHTML(string htmlString)
        {   //http://stackoverflow.com/questions/2334883/how-can-i-strip-html-tags-in-c-sharp
            //http://www.dotnetperls.com/remove-html-tags

            string pattern = @"<(.|\n)*?>";
            return Regex.Replace(htmlString, pattern, string.Empty);
        }

        public void SendMail()
        {   //http://stackoverflow.com/questions/32260/sending-email-in-net-through-gmail

            if (DocumentFileInfo != null && File.Exists(DocumentFileInfo.FullName))
            {
                MailAddress fromAddress = new MailAddress(Settings.Default.EmailAddress, Settings.Default.EmailPersonName);
                MailAddress toAddress = new MailAddress(Settings.Default.KindleEmailAddress, Settings.Default.KindlePersonName);
                String emailPassword = Cryptography.DecryptStringFromBytes_Aes(System.Convert.FromBase64String(Settings.Default.EmailPassword), GetAesKey(), GetAesIV());
                                
                SmtpClient smtp = new SmtpClient
                {
                    Host = Settings.Default.EmailHost,
                    Port = Settings.Default.EmailPort,
                    EnableSsl = Settings.Default.EmailEnableSsl,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, emailPassword)
                };

                using (MailMessage message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = DocumentTitle
                })
                {
                    try
                    {
                        Attachment attachment = new Attachment(DocumentFileInfo.FullName, MediaTypeNames.Application.Octet);

                        ContentDisposition disposition = attachment.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(DocumentFileInfo.FullName);
                        disposition.ModificationDate = File.GetLastWriteTime(DocumentFileInfo.FullName);
                        disposition.ReadDate = File.GetLastAccessTime(DocumentFileInfo.FullName);
                        disposition.FileName = System.IO.Path.GetFileName(DocumentFileInfo.FullName);
                        disposition.Size = new FileInfo(DocumentFileInfo.FullName).Length;
                        disposition.DispositionType = DispositionTypeNames.Attachment;
                        message.Attachments.Add(attachment);

                        //Sending mail asynchronously doesn't throw any exception in any case?!
                        //smtp.SendAsync(message, Title = Settings.Default.ProgramTitle + ": Sending PDF document to your Kindle...");
                        //smtp.SendCompleted += smtp_SendCompleted;
                        Title = Settings.Default.ProgramTitle + ": Sending PDF document to your Kindle...";
                        smtp.Send(message);
                        Title = Settings.Default.ProgramTitle + ": Document has been sent to your Kindle!";
                    }
                    catch (DirectoryNotFoundException ex)
                    {
                        Console.WriteLine("Directory not found: " + ex);
                    }
                    catch (FileNotFoundException ex)
                    {
                        Console.WriteLine("File not found: " + ex);
                    }
                    catch (SmtpException ex)
                    {
                        MessageBox.Show("An error occurred while trying to send the document to your Kindle device!\nPossible reason: Wrong password.\nStatus Code: " + ex.StatusCode.ToString(), "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        Console.WriteLine("SMTP error: " + ex);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unknown error: " + ex);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please generate a PDF file first!");
            }
        }

        void smtp_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            Title = Settings.Default.ProgramTitle + ": Document has been sent to your Kindle!";
            StartTimer();
        }

        private void StartTimer()
        {
            Console.WriteLine("Timer started!");
            timerStart = DateTime.Now;
            timer.Start();
        }
        
        private Boolean CreateHTMLDocument(String outputHtmlFile)
        {
            using (TextWriter writer = new StreamWriter(outputHtmlFile, true, Encoding.UTF8))
            {
                writer.Write(@"<html><body>");
                foreach (RssNews rn in newsList)
                {
                    writer.Write(rn.HTMLRepresentation);
                }
                writer.Write(@"</body></html>");
            }
            return true;
        }

        private Boolean CreateMobiDocument(String inputHtmlFile)
        {
            Process kindleGen = new Process();
            kindleGen.StartInfo.UseShellExecute = false;
            kindleGen.StartInfo.RedirectStandardOutput = true;
            //kindleGen.StartInfo.CreateNoWindow = true;
            //kindleGen.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            String kindleGenFile = Settings.Default.KindleGenLocation;
            if (!File.Exists(kindleGenFile))
            {
                MessageBox.Show("Kindlegen.exe is missing!");
                return false;
            }
            kindleGen.StartInfo.FileName = kindleGenFile;
            kindleGen.StartInfo.Arguments = inputHtmlFile;// String.Format("\"{0}\"", inputHtmlFile);
            kindleGen.Start();
            String output = kindleGen.StandardOutput.ReadToEnd();            
            kindleGen.WaitForExit();
            if (output.ToLower().Contains("error"))
            {
                MessageBox.Show("Couldn't generate MOBI file!\nError:\n" + output);
                return false;
            }
            //File.WriteAllText(outputMobiFile, output);
            return true;
        }

        private Boolean CreatePdfDocument(String pdfFilename)
        {
            int counter = 1;
            iTextSharp.text.Paragraph header, date, author, body;
            iTextSharp.text.Font fontHeader = FontFactory.GetFont(Settings.Default.DocumentHeaderFontType, Settings.Default.DocumentHeaderFontSize, BaseColor.BLACK);
            iTextSharp.text.Font fontBody = FontFactory.GetFont(Settings.Default.DocumentBodyFontType, Settings.Default.DocumentBodyFontSize, BaseColor.BLACK);
            iTextSharp.text.Font fontDate = FontFactory.GetFont(BaseFont.TIMES_ROMAN, 8, BaseColor.DARK_GRAY);

            Document pdfDoc = null;
            try
            {
                pdfDoc = new Document(PdfPageSize);
                PdfWriter.GetInstance(pdfDoc, new FileStream(pdfFilename, FileMode.Create));
            }
            catch (NotSupportedException ex)
            {
                MessageBox.Show("Error creating PDF document!\n Exception: " + ex.ToString());
                return false;
            }
            pdfDoc.Open();
            foreach (RssNews rn in newsList)
            {
                header = new iTextSharp.text.Paragraph(rn.Title, fontHeader);

                date = new iTextSharp.text.Paragraph(rn.PublicationDate.UtcDateTime.ToShortDateString() + " - " + rn.PublicationDate.UtcDateTime.ToShortTimeString(), fontDate);
                date.Alignment = Element.ALIGN_LEFT;

                author = new iTextSharp.text.Paragraph("Authors: " + rn.Author, fontBody);
                author.Alignment = Element.ALIGN_LEFT;

                body = new iTextSharp.text.Paragraph(rn.Description, fontBody);
                body.Alignment = Element.ALIGN_JUSTIFIED & Element.ALIGN_TOP;

                Chapter chapter = new Chapter(header, counter);
                chapter.BookmarkOpen = true;
                chapter.Add(new iTextSharp.text.Paragraph()); //add spacing between header and body
                chapter.Add(date);
                if (rn.Author.Length > 0)
                {
                    chapter.Add(author);
                }
                if (Settings.Default.DocumentWithImages)
                {
                    iTextSharp.text.Image img = null;
                    try
                    {
                        img = iTextSharp.text.Image.GetInstance(rn.ImageUrl);
                    }
                    catch (NotSupportedException ex)
                    {
                        //Console.WriteLine("Invalid URI: " + ex);
                    }
                    catch (ArgumentNullException ex)
                    {

                    }
                    if (img != null)
                    {
                        img.ScaleToFit(150f, 150f);
                        img.Border = iTextSharp.text.Rectangle.BOX;
                        img.BorderColor = BaseColor.BLACK;
                        img.BorderWidth = 1f;
                        img.Alignment = iTextSharp.text.Image.TEXTWRAP | iTextSharp.text.Image.ALIGN_RIGHT;
                        img.IndentationLeft = 9f;
                        img.SpacingAfter = 9f;
                        img.BorderWidthTop = 36f;
                        img.BorderColorTop = BaseColor.WHITE;
                        chapter.Add(img);
                    }
                }
                chapter.Add(body);
                pdfDoc.Add(chapter);
                counter++;
            }
            pdfDoc.Close();
            return true;
        }

        private void CreateDocument()
        {
            String rssUrl = null;
            try
            {
                rssUrl = Settings.Default.RssLinks[this.CmbRssLinks.SelectedIndex];
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Choose an item from the list!");
                Console.WriteLine(ex);
                return;
            }
            if (!FillListWithRssFeedData(rssUrl))
            {
                return;
            }            
            DocumentFormat docFormat = DocumentFormat.PDF;
            Enum.TryParse<DocumentFormat>(Settings.Default.DocumentFormat, out docFormat);
            //docFormat.ToString().ToLower()
            switch (docFormat)
            {
                case DocumentFormat.HTML:
                    SetDocumentFileInfo(DocumentTitle, "html");
                    CreateHTMLDocument(DocumentFileInfo.FullName);
                    break;
                case DocumentFormat.MOBI:
                    SetDocumentFileInfo(DocumentTitle, "html");
                    CreateHTMLDocument(DocumentFileInfo.FullName);
                    CreateMobiDocument(DocumentFileInfo.FullName);
                    break;
                case DocumentFormat.PDF:
                    SetDocumentFileInfo(DocumentTitle, "pdf");
                    CreatePdfDocument(DocumentFileInfo.FullName);
                    break;
            }
            Title = Settings.Default.ProgramTitle + ": " + docFormat + " document created!";
            if (Settings.Default.OpenPdfFileWhenDone)
            {
                Process.Start(DocumentFileInfo.FullName);
            }
        }

        private void SetDocumentFileInfo(String title, String extension)
        {
            DocumentFileInfo = new FileInfo(title + "." + extension);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SendMail();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            StartTimer();
            CreateDocument();
        }

        private void txtRssUrl_Copy_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.DocumentTitle = ((TextBox)sender).Text;
        }

        private void Find_Click(object sender, RoutedEventArgs e)
        {
            BrowserWindow bw = new BrowserWindow();
            bw.Show();
            bw.Closing += bw_Closing;
        }

        void bw_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (((BrowserWindow)sender).UseRssUrl)
                this.RssUrl.Text = Settings.Default.BrowserLastVisitedUrl;
        }

        private void AddRssLink_Click(object sender, RoutedEventArgs e)
        {
            AddRssFeed(this.RssUrl.Text);
        }

        private void AddRssFeed(String rssUrl)
        {
            Uri tempUrl = null;
            Uri.TryCreate(rssUrl, UriKind.Absolute, out tempUrl);
            if (tempUrl == null)
            {
                MessageBox.Show("Please use a valid URL!");
            }
            else
            {
                if (Settings.Default.RssLinks.Contains(rssUrl))
                {
                    MessageBox.Show("RSS feed exists!");
                }
                else
                {
                    Settings.Default.RssLinks.Add(tempUrl.ToString());
                    FillComboBoxWithRssItems();
                }
            }
        }

        private void RemoveRssLink_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.RssUrl.Clear();
                Settings.Default.RssLinks.RemoveAt(this.CmbRssLinks.SelectedIndex);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex);
            }
            FillComboBoxWithRssItems();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Settings.Default.Save();
            //Application.Current.Shutdown();
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            new SettingsWindow(AesMan).ShowDialog();
            Settings.Default.Reload();
        }

        private void CmbRssLinks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cmbRssUrl = sender as ComboBox;
            String currentRssUrlTxtSelVal = RssUrl.Text;
            String currentRssUrlCmbSelVal = null;
            try {
                currentRssUrlCmbSelVal = e.AddedItems[0].ToString();
            }
            catch(IndexOutOfRangeException ex) {
                Console.WriteLine(ex);
                return;
            }
            if (currentRssUrlTxtSelVal.Trim().Length > 0 && !cmbRssUrl.Items.Contains(currentRssUrlTxtSelVal))
            {
                MessageBoxResult result = MessageBox.Show("The current RSS Feed URL in the text box isn't in the feed\'s list!\n\nWould you like to add this URL to the list?", "RSS Feed not in list...", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
                if (result == MessageBoxResult.Yes)
                {
                    AddRssFeed(currentRssUrlTxtSelVal);
                }
            }
            RssUrl.Text = currentRssUrlCmbSelVal;
        }
    }
}
