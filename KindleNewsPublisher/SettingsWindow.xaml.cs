﻿using KindleNewsPublisher.Classes;
using KindleNewsPublisher.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KindleNewsPublisher
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        private Boolean m_IsSaved = true;
        private AesManaged AesMan;
        private byte[] encryptedPassword;
        private String tempPassword;

        public SettingsWindow()
        {
            InitializeComponent();
        }

        public SettingsWindow(AesManaged _aesMan)
            : this()
        {
            AesMan = _aesMan;
            Init();
        }

        private void Init()
        {
            KindleEmail.Text = Settings.Default.KindleEmailAddress;
            ChkOpenPdfFileWhenDone.IsChecked = Settings.Default.OpenPdfFileWhenDone;
            ChkDocumentIncludeImages.IsChecked = Settings.Default.DocumentWithImages;
            cmbDocumentFormat.ItemsSource = Enum.GetValues(typeof(DocumentFormat));
            cmbDocumentFormat.SelectedItem = Enum.Parse(typeof(DocumentFormat), Settings.Default.DocumentFormat);

            EmailAddress.Text = Settings.Default.EmailAddress;
            EmailPassword.Password = Settings.Default.EmailPassword;


            TxtDocumentHeaderFontSize.Text = Settings.Default.DocumentHeaderFontSize.ToString();
            TxtDocumentBodyFontSize.Text = Settings.Default.DocumentBodyFontSize.ToString();
            CmbDocumentHeaderFontType.ItemsSource = iTextSharp.text.FontFactory.RegisteredFonts;
            CmbDocumentBodyFontType.ItemsSource = iTextSharp.text.FontFactory.RegisteredFonts;

            CmbDocumentHeaderFontType.SelectedItem = Settings.Default.DocumentHeaderFontType;
            CmbDocumentBodyFontType.SelectedItem = Settings.Default.DocumentBodyFontType;

            TxtKindleGenLocation.Text = Settings.Default.KindleGenLocation;

            Settings.Default.SettingChanging += Default_SettingChanging;

        }

        void Default_SettingChanging(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            m_IsSaved = false;
        }

        private void SaveSettings()
        {

            Settings.Default.KindleEmailAddress = KindleEmail.Text;
            Settings.Default.OpenPdfFileWhenDone = ChkOpenPdfFileWhenDone.IsChecked.Value;
            Settings.Default.DocumentWithImages = ChkDocumentIncludeImages.IsChecked.Value;
            Settings.Default.DocumentFormat = cmbDocumentFormat.SelectedItem.ToString();
            Settings.Default.EmailAddress = EmailAddress.Text;
            Int32 docHeaderFontSize = 14, docBodyFontSize = 10;
            Int32.TryParse(TxtDocumentHeaderFontSize.Text, out docHeaderFontSize);
            Settings.Default.DocumentHeaderFontSize = docHeaderFontSize;
            Int32.TryParse(TxtDocumentBodyFontSize.Text, out docBodyFontSize);
            Settings.Default.DocumentBodyFontSize = docBodyFontSize;
            Settings.Default.DocumentHeaderFontType = CmbDocumentHeaderFontType.Text;
            Settings.Default.DocumentBodyFontType = CmbDocumentBodyFontType.Text;
            Settings.Default.KindleGenLocation = TxtKindleGenLocation.Text;

            //String oldPassword = Cryptography.DecryptStringFromBytes_Aes(System.Convert.FromBase64String(Settings.Default.EmailPassword), AesMan.Key, AesMan.IV);
            //String newPassword = EmailPassword.Password;



            //MessageBox.Show(EmailPassword.Password + "\n" + Settings.Default.EmailPassword);

            /*
            //MessageBox.Show(Settings.Default.EmailPassword);
            //MessageBox.Show(Cryptography.DecryptStringFromBytes_Aes(System.Convert.FromBase64String(Settings.Default.EmailPassword), AesMan.Key, AesMan.IV));
            try
            {
                encryptedPassword = System.Convert.FromBase64String(Settings.Default.EmailPassword);
                MessageBox.Show(Encoding.UTF8.GetString(encryptedPassword));
                //EmailPassword.Password = Encoding.UTF8.GetString(encryptedPassword);

                MessageBox.Show(Cryptography.DecryptStringFromBytes_Aes(System.Convert.FromBase64String(Settings.Default.EmailPassword), AesMan.Key, AesMan.IV));
                EmailPassword.Password = Cryptography.DecryptStringFromBytes_Aes(System.Convert.FromBase64String(Settings.Default.EmailPassword), AesMan.Key, AesMan.IV);

                //MessageBox.Show(Encoding.UTF8.GetString(encryptedPassword));


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }*/

            if (!Settings.Default.EmailPassword.Equals(EmailPassword.Password))
            {
                encryptedPassword = Cryptography.EncryptStringToBytes_Aes(EmailPassword.Password, AesMan.Key, AesMan.IV);
                String newPassword = System.Convert.ToBase64String(encryptedPassword);
                //MessageBox.Show(newPassword);
                //MessageBox.Show("Change!\n\n" + Settings.Default.EmailPassword + "\n" + newPassword);
                //encryptedPassword = Cryptography.EncryptStringToBytes_Aes(EmailPassword.Password, AesMan.Key, AesMan.IV);
                Settings.Default.EmailPassword = newPassword;// System.Convert.ToBase64String(encryptedPassword);
            }
            m_IsSaved = true;
            Settings.Default.Save();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveSettings();
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBlock_MouseEnter_1(object sender, MouseEventArgs e)
        {
            TextBlock Label = (TextBlock)sender;
            Label.Foreground = Brushes.Blue;
            Label.Cursor = Cursors.Hand;
            Label.FontWeight = FontWeights.Bold;
        }

        private void TextBlock_MouseLeave_1(object sender, MouseEventArgs e)
        {
            TextBlock Label = (TextBlock)sender;
            Label.Foreground = Brushes.Black;
            Label.Cursor = Cursors.Arrow;
            Label.FontWeight = FontWeights.Normal;
        }

        private void TextBlock_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            new BrowserWindow("https://www.amazon.com/gp/digital/fiona/manage#pdocSettings").ShowDialog();
        }

        private void Window_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.Close();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (m_IsSaved)
                return;
            MessageBoxResult result = MessageBox.Show("Save the settings?", "Save or discard changes?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
            if (result == MessageBoxResult.No)
                return;
            else
                SaveSettings();
        }

        private void BtnSelectKindleGenLocation_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Environment.CurrentDirectory;
            ofd.Filter = "Program files(*.exe)|*.exe|All files (*.*)|*.*";
            ofd.RestoreDirectory = true;
            ofd.Multiselect = false;
            ofd.ShowDialog();
            String file = ofd.FileName;
            if (File.Exists(file))
            {
                this.TxtKindleGenLocation.Text = ofd.FileName;
            }
            else
            {
                MessageBox.Show("Not a valid file!");
            }
        }


    }
}
