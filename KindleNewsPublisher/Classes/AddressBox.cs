﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace KindleNewsPublisher.Classes
{
    public class AddressBox : TextBox
    {
        public event EventHandler UrlChanged;

        private Uri m_URL = null;
        public Uri URL
        {
            get
            {
                return m_URL;
            }
            set
            {
                m_URL = value;
                if (this.UrlChanged != null)
                {
                    this.UrlChanged(this, new EventArgs());
                }
            }
        }

        public AddressBox()
            : base()
        {
            this.UrlChanged += AddressBox_UrlChanged;
        }

        private void AddressBox_UrlChanged(object sender, EventArgs e)
        {
            if (m_URL == null)
                return;
            this.Text = m_URL.ToString();
        }
    }
}
