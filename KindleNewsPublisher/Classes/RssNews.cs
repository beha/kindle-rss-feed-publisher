﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KindleNewsPublisher.Classes
{
    public class RssNews : IMobi
    {
        public Int32 ID { get; set; }
        public String Title { get; set; }
        public String Author { get; set; }
        public String Description { get; set; }
        public Uri ImageUrl { get; set; }
        public DateTimeOffset PublicationDate { get; set; }

        public string HTMLRepresentation
        {
            get
            {
                return String.Format(@"
                        <h1>{0}</h1>
                        <h4>By {1} - on {2}</h4>
                        <p>{3}</p>", Title, Author, PublicationDate.UtcDateTime.ToShortDateString() + " - " + PublicationDate.UtcDateTime.ToShortTimeString(), Description);
            }
        }

        public RssNews()
        {

        }

        public RssNews(String _title, String _author, String _description, DateTimeOffset _publicationDate, Uri _imageUrl)
        {
            Title = _title;
            Author = _author;
            Description = _description;
            PublicationDate = _publicationDate;
            ImageUrl = _imageUrl;
        }
    }
}
