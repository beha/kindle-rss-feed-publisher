﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KindleNewsPublisher.Classes
{
    public enum DocumentFormat
    {
        PDF,
        MOBI,
        HTML
    }
}
