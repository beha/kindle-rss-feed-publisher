﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KindleNewsPublisher
{
    public interface IMobi
    {
        int ID { get; }
        string HTMLRepresentation { get; }
    }
}
