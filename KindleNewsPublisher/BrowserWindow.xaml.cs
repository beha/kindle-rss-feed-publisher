﻿using Awesomium.Windows.Controls;
using KindleNewsPublisher.Classes;
using KindleNewsPublisher.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KindleNewsPublisher
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class BrowserWindow : Window
    {
        private Boolean m_UseRssUrl = false;

        public Boolean UseRssUrl
        {
            get { return m_UseRssUrl; }
            set { m_UseRssUrl = value; }
        }
        private AddressBox addressBox = new AddressBox();

        public BrowserWindow()
        {
            InitializeComponent();
            Init();
        }

        public BrowserWindow(String url)
            : this()
        {
            Navigate(url);
        }

        private void Navigate(string url)
        {
            try
            {
                Browser.Source = new Uri(url);
            }
            catch (Exception ex) { } 
        }

        private void Init()
        {
            m_UseRssUrl = false;
            addressBox.KeyDown += addressBox_KeyDown;
            AddressContainer.Children.Add(addressBox);
            Navigate(Settings.Default.BrowserLastVisitedUrl);
            Browser.LoadingFrame += Browser_LoadingFrame;
        }

        void Browser_LoadingFrame(object sender, Awesomium.Core.LoadingFrameEventArgs e)
        {
            addressBox.URL = e.Url;
            Title = e.Url.ToString();
            Settings.Default.BrowserLastVisitedUrl = e.Url.ToString();
        }


        private void AskUserAboutRssUrl()
        {
            UseRssUrl = false;
            try
            {
                Settings.Default.Save();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't save the 'browsers visited used URL'");
            }

            MessageBoxResult result = MessageBox.Show("Use the URL in address bar as RSS Feed URL?", "Use the URL?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes);
            if (result == MessageBoxResult.Yes)
                UseRssUrl = true;
        }

        void addressBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Navigate("https://www.google.com/search?q=" + this.addressBox.Text + " inurl:rss|feed");
            }
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            AskUserAboutRssUrl();
        }

        private void Window_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.Close();
        }
    }
}
